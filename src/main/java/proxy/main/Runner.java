package proxy.main;

import java.util.HashMap;

import proxy.dao.PersonDAO;
import proxy.dao.impl.PersonDAOImpl;
import proxy.domain.Person;

public class Runner {
	public static void main(String[] args) {
		PersonDAO personDAO = new PersonDAOImpl(new HashMap<String, Person>(), "persons.txt");
		System.err.println(personDAO.readPerson("Jack"));
	}
}
