package proxy.dao.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

import proxy.dao.PersonDAO;
import proxy.dao.stream.PersonInputStream;
import proxy.dao.stream.impl.PersonInputStreamImpl;
import proxy.domain.Person;

public class PersonDAOImpl implements PersonDAO {

	private Map<String, Person> cache;
	private String filename;

	public PersonDAOImpl(Map<String, Person> cache, String filename) {
		this.cache = cache;
		this.filename = filename;
	}

	public Person readPerson(String name) {
		Person person = cache.get(name);
		if (person == null) {
			person = readFromFile(name);
			cache.put(name, person);
		}
		return person;
	}

	private Person readFromFile(String personName) {
		PersonInputStream personInputStream = null;
		Person person = null;
		try {
			personInputStream = new PersonInputStreamImpl(new FileInputStream(new File(filename)));
			person = personInputStream.readPerson(personName);
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			if (personInputStream != null) {
				try {
					personInputStream.close();
				} catch (IOException e) {
					System.err.println(e);
				}
			}
		}
		return person;
	}
}
