package proxy.dao;

import proxy.domain.Person;

public interface PersonDAO {
	Person readPerson(String name);
}
