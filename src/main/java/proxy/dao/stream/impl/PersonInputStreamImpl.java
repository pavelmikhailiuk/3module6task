package proxy.dao.stream.impl;

import java.io.IOException;
import java.io.InputStream;

import proxy.dao.stream.PersonInputStream;
import proxy.domain.Person;

public class PersonInputStreamImpl implements PersonInputStream {

	private InputStream inputStream;

	public PersonInputStreamImpl(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public Person readPerson(String name) throws IOException {
		StringBuilder sb = new StringBuilder();
		int i = 0;
		while ((i = inputStream.read()) != -1) {
			char c = (char) i;
			sb.append(c);
		}
		return initPerson(sb.toString(), name);
	}

	public void close() throws IOException {
		inputStream.close();
	}

	private Person initPerson(String personData, String name) {
		String[] persons = personData.split(",");
		Person person = null;
		for (String psn : persons) {
			if (psn.equals(name)) {
				person = new Person(psn);
			}
		}
		return person;
	}
}
