package proxy.dao.stream;

import java.io.IOException;

import proxy.domain.Person;

public interface PersonInputStream {

	Person readPerson(String name) throws IOException;

	void close() throws IOException;
}
